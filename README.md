# MLCVZoo Tracker

The MLCVZoo is an SDK for simplifying the usage of various (machine learning driven)
computer vision algorithms. The package **mlcvzoo_tracker** contains tools for tracking detected objects.

## Install
`
pip install mlcvzoo-tracker
`

## Technology stack

- Python

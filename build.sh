#!/bin/sh

TARGET_PYTHON_VERSION=$(python3 -c 'import platform;print("".join(platform.python_version().split(".")[0:2]))')

REQUIREMENTS_FILE="${REQUIREMENTS_FILE:=./requirements_locked/requirements-lock-uv-py$TARGET_PYTHON_VERSION-all.txt}"

# Sync dependencies with uv, this will remove all packages, that are not part of the .txt file
#uv pip sync "$REQUIREMENTS_FILE"
# Temporary use direct pip install instead of sync, due to network issues
# TODO: Use uv sync again, one it is stable
python3 -m pip install -r "$REQUIREMENTS_FILE"

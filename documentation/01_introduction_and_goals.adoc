[[section-introduction]]

== Introduction

*mlcvzoo_tracker* provides an implementation of an Multi-Object Tracker based on bounding boxes that can be provided by an mlcvzoo ObjectDetectionModel.
[[section-tutorial]]

== Tutorial

Refer to the link:https://gitlab.cc-asp.fraunhofer.de/silicon-economy/base/ml-toolbox/mlcvzoo-base/-/tree/main/documentation[MLCVZoo Base documentation] for more information.

=== Installation

Install with pip:

-------
pip install mlcvzoo-tracker
-------